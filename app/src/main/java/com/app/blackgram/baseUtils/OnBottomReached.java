package com.app.blackgram.baseUtils;

/**
 * Created by KrishnaDev on 1/10/17.
 */
public interface OnBottomReached {
    void onBottomReached();
}
